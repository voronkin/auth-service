<?php declare(strict_types=1);

namespace App\Presentation\Controller;

use App\Presentation\Controller\Request\TrackingAction;
use App\Presentation\Controller\Traits\ResponseControllerTrait;
use App\Presentation\Messenger\Message\TrackingActionMessage;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class TrackingActionController extends AbstractController
{
    use ResponseControllerTrait;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $bus;

    /**
     * @var Security
     */
    private Security $security;


    /**
     * TrackingUserActionController constructor.
     *
     * @param MessageBusInterface $bus
     * @param Security            $security
     */
    public function __construct(MessageBusInterface $bus, Security $security)
    {
        $this->bus = $bus;
        $this->security = $security;
    }

    /**
     * @Route("/api/v1/user/tracking-action", methods={"POST"},  name="api.v1.user.tracking-action")
     * @SWG\Parameter(
     *         name="TrackingAction",
     *         required=true,
     *         in="body",
     *         @Model(type=TrackingAction::class)
     *     )
     * @SWG\Parameter(name="X-AUTH-TOKEN", in="header", type="string", required=true)
     * @SWG\Response(
     *      response=201,
     *      description="Tracking action success",
     * )
     *
     *
     * @param TrackingAction $trackingAction
     *
     * @return JsonResponse
     */
    public function trackingAction(TrackingAction $trackingAction): JsonResponse
    {
        $this->bus->dispatch(
            new TrackingActionMessage(
                $this->security->getUser()->getId(),
                $trackingAction->getSourceType(),
                $trackingAction->getDateCreated()
            )
        );

        return new JsonResponse(null, 201);
    }
}
