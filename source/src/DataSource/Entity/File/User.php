<?php declare(strict_types=1);

namespace App\DataSource\Entity\File;

use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_GUEST = 'ROLE_GUEST';

    /**
     * @var string
     */
    private string $id;

    /**
     * @var string|null
     */
    private ?string $firstName = null;

    /**
     * @var string|null
     */
    private ?string $lastName = null;

    /**
     * @var string|null
     */
    private ?string $nickName = null;

    /**
     * @var int|null
     */
    private ?int $age = null;

    /**
     * @var string|null
     */
    private ?string $password = null;

    /**
     * @var string
     */
    private string $token;

    /**
     * @var DateTime
     */
    private DateTime $tokenExpired;

    /**
     * @var array
     */
    private array $roles;

    public function __construct()
    {
        $this->id = uuid_create(UUID_TYPE_TIME);
        $this->token = md5($this->id);
        $this->tokenExpired = new DateTime('+1 day');
        $this->nickName = 'Guest';
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return User
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return User
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return User
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    /**
     * @param string|null $nickName
     *
     * @return User
     */
    public function setNickName(?string $nickName): self
    {
        $this->nickName = $nickName;

        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int|null $age
     *
     * @return User
     */
    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     *
     * @return User
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     *
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return User
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTokenExpired(): DateTime
    {
        return $this->tokenExpired;
    }

    /**
     * @param DateTime $tokenExpired
     *
     * @return User
     */
    public function setTokenExpired(DateTime $tokenExpired): self
    {
        $this->tokenExpired = $tokenExpired;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->nickName;
    }

    public function eraseCredentials(): void
    {

    }
}
