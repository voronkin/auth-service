<?php declare(strict_types=1);

namespace App\DataSource\Repository;

use App\DataSource\Entity\File\User;
use App\DataSource\Storage\File\FileStorageInterface;
use App\Domain\User\TokenGenerator;
use DateTime;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Serializer\SerializerInterface;

class UserRepository
{
    private const ENTITY_PATH_PREFIX = 'user';

    /**
     * @var FileStorageInterface
     */
    private FileStorageInterface $storage;

    /**
     * @var string
     */
    private string $baseFileEntityPath;

    /**
     * @var TokenGenerator
     */
    private TokenGenerator $tokenGenerator;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @param string               $baseFileEntityPath
     * @param FileStorageInterface $storage
     * @param TokenGenerator       $tokenGenerator
     * @param SerializerInterface  $serializer
     */
    public function __construct(
        string $baseFileEntityPath,
        FileStorageInterface $storage,
        TokenGenerator $tokenGenerator,
        SerializerInterface $serializer
    ) {
        $this->storage = $storage;
        $this->baseFileEntityPath = $baseFileEntityPath;
        $this->tokenGenerator = $tokenGenerator;
        $this->serializer = $serializer;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $nickName
     * @param int    $age
     * @param string $password
     *
     * @return User
     */
    public function create(
        string $firstName,
        string $lastName,
        string $nickName,
        int $age,
        string $password
    ): User {
        $user = (new User())
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setNickName($nickName)
            ->setAge($age)
            ->setRoles([User::ROLE_USER])
            ->setPassword(md5($password))
            ->setToken($this->tokenGenerator->generateToken());

        return $this->storage->write(
            $this->getFullPath($user),
            $this->serializer->serialize($user, 'json')
        );
    }

    /**
     * @param User $user
     * @param      $firstName
     * @param      $lastName
     * @param      $nickName
     * @param      $age
     * @param      $password
     */
    public function updateUser(
        User $user,
        string $firstName,
        string $lastName,
        string $nickName,
        int $age,
        string $password
    ): void {
        $this->storage->remove($this->getFullPath($user));

        $user
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setNickName($nickName)
            ->setAge($age)
            ->setPassword(md5($password))
            ->setRoles([User::ROLE_USER])
            ->setToken($this->tokenGenerator->generateToken())
            ->setTokenExpired(new DateTime('+1 day'));

        $this->storage->write(
            $this->getFullPath($user),
            $this->serializer->serialize($user, 'json')
        );
    }

    /**
     * @return User
     */
    public function createGuest(): User
    {
        $user = (new User())
            ->setRoles([User::ROLE_GUEST]);

        $this->storage->write(
            $this->getFullPath($user),
            $this->serializer->serialize($user, 'json')
        );

        return $user;
    }

    /**
     * @param User   $user
     * @param string $token
     */
    public function updateToken(User $user, string $token): void
    {
        $this->storage->remove($this->getFullPath($user));

        $user
            ->setToken($token)
            ->setTokenExpired(new DateTime('+1 day'));

        $this->storage->write(
            $this->getFullPath($user),
            $this->serializer->serialize($user, 'json')
        );
    }

    /**
     * @param string $token
     *
     * @return User
     */
    public function getByToken(string $token): User
    {
        $files = $this->storage->findFileByPattern($this->getBasePath(),
            $token.'.*');

        if (count($files) != 1) {
            throw new UsernameNotFoundException('User not found', 401);
        }

        return $this->serializer->deserialize(
            $this->storage->read($this->getBasePath().'/'.$files[0]),
            User::class,
            'json'
        );
    }

    /**
     * @param string $nickName
     *
     * @return User
     */
    public function getByNickName(string $nickName): User
    {
        $files = $this->storage->findFileByPattern($this->getBasePath(),
            '*'.'.'.md5($nickName));

        if (count($files) != 1) {
            throw new UsernameNotFoundException('User not found', 401);
        }

        return $this->serializer->deserialize(
            $this->storage->read($this->getBasePath().'/'.$files[0]),
            User::class,
            'json'
        );
    }

    /**
     * @param string $nickName
     *
     * @return bool
     */
    public function isNickNameUnique(string $nickName): bool
    {
        $files = $this->storage->findFileByPattern($this->getBasePath(),
            '*'.'.'.md5($nickName));

        return count($files) ? false : true;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    private function getFullPath(User $user): string
    {
        return $this->getBasePath().
            '/'.$user->getToken().'.'.md5($user->getNickName());
    }

    /**
     * @return string
     */
    private function getBasePath(): string
    {
        return $this->baseFileEntityPath.'/'.self::ENTITY_PATH_PREFIX;
    }
}
